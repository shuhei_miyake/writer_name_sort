//
//  TargetView.h
//  Writer_name_sort
//
//  Created by Shuhei on 2014/07/28.
//  Copyright (c) 2014年 三宅 修平. All rights reserved.
//

#import <UIKit/UIKit.h>
@class DragLabel;

@interface TargetView : UIView{
    
    DragLabel *label;
    Boolean isCorrect;
    NSString *correct_word;
}

@property (strong) DragLabel* label;
@property (strong) NSString* correct_word;
@property  Boolean isCorrect;

@end
