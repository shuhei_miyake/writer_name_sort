//
//  AppDelegate.m
//  Writer_name_sort
//
//  Created by Shuhei on 2014/07/14.
//  Copyright (c) 2014年 三宅 修平. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate
@synthesize appDict = _appDict;


-(NSMutableArray *) getCategory
{
    return category;
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.appDict=[[NSMutableDictionary alloc] init];
    [self dataBaseOpen];
    questions = [[NSMutableArray alloc]init];
    
    // Override point for customization after application launch.
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(void) dataBaseOpen
{
        //dbが存在してなかったらここが呼ばれて、作成したDBをコピー
        NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"dbtest.db"];
    
    //作成したテーブルからデータを取得
     db = [FMDatabase databaseWithPath:defaultDBPath];
    if ([db open]) {
        [db setShouldCacheStatements:YES];
        
        FMResultSet *rs = [db executeQuery:@"SELECT name FROM sqlite_master WHERE type='table' and name!='sqlite_sequence'"];
        category = [[NSMutableArray alloc]init];
        while ([rs next]) {
            //ここでデータを展開
            NSLog(@"%@", [rs stringForColumn:@"name"]);
            [category addObject:[rs stringForColumn:@"name"]];
        }
        [appDict setObject:category forKey:@"category"];
        [rs close];
        [db close];
    }else{
        //DBが開けなかったらここ
    }
  
    
}

-(void)setQuestions:(int)category_number
{
    NSString *category_name = category[category_number];
    NSString *query =[NSString stringWithFormat:@"SELECT * FROM %@",category_name];
    [db open];
    FMResultSet *rs = [db executeQuery:query];
    while([rs next]){
    
        person = [[NSMutableArray alloc]init];
        [person addObject:[rs stringForColumn:@"id"]];
        [person addObject:[rs stringForColumn:@"name"]];
        [person addObject:[rs stringForColumn:@"data"]];
        [person addObject:[rs stringForColumn:@"pic_num"]];
        [questions addObject:person];
        
    }
    
}

-(NSMutableArray *)getQuestions{
    return questions;
}

-(void)resetQuestions{

    [questions removeAllObjects];

}

-(void)setLevel:(int)l{
 
    switch(l){
        case 0 : level = 5; break;
        case 1 : level = 4; break;
        case 2 : level = 2; break;
    }
    

}

-(int)getLevel{

    return level;
    
    
}

@end
