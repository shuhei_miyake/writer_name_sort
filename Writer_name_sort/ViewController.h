//
//  ViewController.h
//  Writer_name_sort
//
//  Created by Shuhei on 2014/07/14.
//  Copyright (c) 2014年 三宅 修平. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "data_set.h"
@interface ViewController : UIViewController{
    
    AppDelegate *appDelegate;
    NSMutableArray *labels;
    NSMutableArray *wordList;
    NSMutableArray *Targets;
    NSMutableSet *label_set;
    NSString *substr;
    CGRect SIZE;
    UIImageView *photo;
    UIView *WorkSpace;
    NSArray *name;
    NSMutableArray *questions;
    data_set *data_Set;
    Boolean isComplete;
}
@property (weak, nonatomic) IBOutlet UILabel *collect;

@property (weak, nonatomic) IBOutlet UILabel *Work_space;
@property (weak, nonatomic) IBOutlet UITextView *text_area;
@property (weak, nonatomic) IBOutlet UIButton *JudgeButon;
@property (weak, nonatomic) IBOutlet UIButton *HintButton;
@property data_set *data_Set;
@property (strong, nonatomic) IBOutlet UIView *WorkSpace;
@property (weak, nonatomic) IBOutlet UIImageView *photo;
@property (retain) NSMutableArray *labels;
@property (retain) NSMutableArray *wordList;
@property (retain) NSMutableArray *questions;
@property (retain) NSMutableArray *Targets;
@property (retain) NSMutableSet *label_set;
@property (retain) NSArray *name;
- (IBAction)judge:(id)sender;
- (IBAction)Hint:(id)sender;
- (IBAction)back:(id)sender;


@end
