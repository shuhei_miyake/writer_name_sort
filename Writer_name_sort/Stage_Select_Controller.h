//
//  Stage_Select_Controller.h
//  Writer_name_sort
//
//  Created by Shuhei on 2014/08/05.
//  Copyright (c) 2014年 三宅 修平. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "MyCell.h"

@interface Stage_Select_Controller : UIViewController<UITableViewDelegate,UITableViewDataSource>{
    
    NSMutableArray *data;
    AppDelegate* appDelegate;
    UITableView* Table;
    NSMutableArray *labels;
}

@property (weak, nonatomic) IBOutlet UITableView *Table;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segment;

@property NSMutableArray *data;

@end
