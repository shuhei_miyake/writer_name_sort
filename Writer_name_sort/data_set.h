//
//  data_set.h
//  Writer_name_sort
//
//  Created by Shuhei on 2014/07/31.
//  Copyright (c) 2014年 三宅 修平. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface data_set : NSObject{
    NSString *name;
    NSString *data;
    UIImage *picture;
}


@property NSString *name;
@property NSString *data;
@property UIImage *picture;


-(void)set:(NSMutableArray*)array;

@end
