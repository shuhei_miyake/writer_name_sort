//
//  DragLabel.m
//  Writer_name_sort
//
//  Created by Shuhei on 2014/07/17.
//  Copyright (c) 2014年 三宅 修平. All rights reserved.
//

#import "DragLabel.h"
#import "TargetView.h"
@implementation DragLabel
@synthesize Views;
@synthesize t_view;
@synthesize isCorrect;
- (id)initWithFrame:(CGRect)frame set:(NSMutableArray *)set
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        set = Views;
        isCorrect = false;
    }
    return self;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */
- (void) touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event
{
	startLocation = [[touches anyObject] locationInView:self];
	[[self superview] bringSubviewToFront:self];
    
    if([self isTouch]!=-1){
        int i = [self isTouch];
        TargetView* target = (TargetView*)Views[i];
        CGRect frame = target.frame;
        [self setFrame:frame];
        NSString* fragment = self.text;
        [self setIscorrect:false];
        self.t_view.isCorrect = false;
        target.label = nil;
        self.t_view = nil;
    }
    
}

- (void) touchesMoved:(NSSet*)touches withEvent:(UIEvent*)event
{
	CGPoint pt = [[touches anyObject] locationInView:self];
	CGRect frame = [self frame];
	frame.origin.x += pt.x - startLocation.x;
	frame.origin.y += pt.y - startLocation.y;
	[self setFrame:frame];
    for(int i=0;i<[Views count];i++){
        if([self isTouch]!=-1){
            int i =  [self isTouch];
            self.layer.borderColor = [UIColor redColor].CGColor;
            TargetView* op = self.Views[i];
            op.layer.borderColor = [UIColor redColor].CGColor;
            
        }else{
            self.layer.borderColor = [UIColor blackColor].CGColor;
            for(int i=0;i<[self.Views count];i++){
                TargetView* op = self.Views[i];
                op.layer.borderColor = [UIColor blackColor].CGColor;
            }
        }
    }
    [self isInside];
    
    
    
}

-(void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if([self isTouch]!=-1){
        int i = [self isTouch];
        TargetView* target = (TargetView*)Views[i];
        CGRect frame = target.frame;
        if(target.label==nil){
            [self setFrame:frame];
            target.label = self;
            self.t_view = target;
            [self isCorrect];
        }else{
            [self setFrame:CGRectMake(self.frame.origin.x, target.frame.origin.y - 25, self.frame.size.width, self.frame.size.height)];
        }
    }
    
}



-(int) isTouch
{
    int value = -1;
    CGRect frame = [self frame];
    CGPoint self_center = CGPointMake(frame.origin.x + frame.size.width/2 , frame.origin.y + frame.size.height/2);
    
    for(int i=0;i<[Views count];i++){
        TargetView* op = (TargetView*) Views[i];
        CGRect op_frame = [op frame];
        CGPoint op_f_point = CGPointMake(op_frame.origin.x, op_frame.origin.y);
        CGPoint op_b_point = CGPointMake(op_frame.origin.x + op_frame.size.width, op_frame.origin.y + op_frame.size.height);
        
        
        if(self_center.x>op_f_point.x && self_center.x<op_b_point.x && self_center.y>op_f_point.y && self_center.y<op_b_point.y)
        {
            value = i;
        }
        
        
    }
    return value;
}

-(void)isCorrect{

    if([self.text isEqualToString:self.t_view.correct_word]){
        isCorrect = 1;
        t_view.isCorrect = 1;
    }else{
        isCorrect = 0;
        t_view.isCorrect = 0;
    }

}

-(void)setIscorrect:(Boolean)b{

    isCorrect = b;

}

-(void) isInside
{
    CGRect frame = [self frame];
    CGRect target;
    if(frame.origin.x<20){
        target = CGRectMake(20, frame.origin.y, frame.size.width, frame.size.height);
        [self setFrame:target];
    }else if(frame.origin.x+frame.size.width>300){
        target = CGRectMake(300-frame.size.width, frame.origin.y, frame.size.width, frame.size.height);
        [self setFrame:target];
    }else if(frame.origin.y<285){
        target = CGRectMake(frame.origin.x, 285, frame.size.width, frame.size.height);
        [self setFrame:target];
        
    }else if(frame.origin.y+frame.size.height>285+243){
        target = CGRectMake(frame.origin.x, 285+243-frame.size.height, frame.size.width, frame.size.height);
        [self setFrame:target];
    }
    


        }


@end
