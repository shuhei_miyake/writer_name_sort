//
//  AppDelegate.h
//  Writer_name_sort
//
//  Created by Shuhei on 2014/07/14.
//  Copyright (c) 2014年 三宅 修平. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FMDatabase.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    NSMutableDictionary *appDict;
    NSMutableArray *category;
    FMDatabase *db;
    NSMutableArray *questions;
    NSMutableArray *person;
    int level;
}
@property int level;
@property(strong, nonatomic) NSMutableArray *category;
@property(strong, nonatomic) NSMutableArray *questions;
@property(strong, nonatomic) NSMutableDictionary *appDict;
@property (strong, nonatomic) UIWindow *window;
-(int)getLevel;
-(void)setLevel;
-(NSMutableArray *) getCategory;
-(void) setQuestions:(int)category;
-(NSMutableArray *) getQuestions;
-(void)resetQuestions;
@end
