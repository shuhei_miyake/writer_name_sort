//
//  DragLabel.h
//  Writer_name_sort
//
//  Created by Shuhei on 2014/07/17.
//  Copyright (c) 2014年 三宅 修平. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TargetView.h"

@interface DragLabel : UILabel{

    CGPoint startLocation;
    NSMutableArray *Views;
    TargetView *t_view;
    Boolean isCorrect;
}
@property NSMutableArray *Views;
@property TargetView *t_view;
@property Boolean isCorrect;
@end
