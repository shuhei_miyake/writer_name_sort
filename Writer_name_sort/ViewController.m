//
//  ViewController.m
//  Writer_name_sort
//
//  Created by Shuhei on 2014/07/14.
//  Copyright (c) 2014年 三宅 修平. All rights reserved.
//

#import "ViewController.h"
#import "DragLabel.h"
#import "TargetView.h"
#import "FMDatabase.h"
#import "data_set.h"
@interface ViewController ()

@end

@implementation ViewController

 int level;
- (void)viewDidLoad
{
    [super viewDidLoad];
    isComplete = false;
    _Work_space = [[UILabel alloc]init];
    data_Set = [[data_set alloc]init];
     appDelegate = [[UIApplication sharedApplication] delegate];
    level = [appDelegate getLevel];
    questions = [[NSMutableArray alloc]init];
    questions = [appDelegate getQuestions];
    //問題をランダムに並べる
    int count = [questions count];
    for(int i= count-1;i>0;i--){
        int randomNum = arc4random() % i;
        [questions exchangeObjectAtIndex:i withObjectAtIndex:randomNum];
    }
    photo = [[UIImageView alloc]initWithFrame:CGRectMake(78,24,165,192)];
    [self.view addSubview:WorkSpace];
    [self.WorkSpace setClipsToBounds:YES];
    NSMutableArray* a = questions[0];
    [data_Set set:a];
    [self createQuestion:data_Set];
    NSLog(@"%d",level);
}

-(void)createQuestion:(data_set*)person{
int frag_number=0;
    Targets = [[NSMutableArray alloc]init];
    _text_area.text = person.data;
    [_text_area sizeToFit];
    photo.image = person.picture;
    [self.view addSubview:photo];

    
    _collect.hidden = YES;
    //名前フラグさくせいa
    name = [[NSArray alloc]init];
    name = [person.name componentsSeparatedByString:@" "];
    wordList = [[NSMutableArray alloc]init];
    for(int i=0;i<[name count];i++){
    srand((unsigned)time(NULL));
        NSString *whole_name = name[i];
        NSMutableArray *fragment = [[NSMutableArray alloc]init];
        while(whole_name.length != 0){
           int ran = (random() % (level))+1;
            if(ran>whole_name.length){ran =whole_name.length;}
            substr = [whole_name substringToIndex:ran];
            [fragment addObject:substr];
            whole_name = [whole_name substringFromIndex:ran];
            frag_number++;
        }
        [wordList addObject:fragment];
        
    }
 
    
 //ラベル作成
    
    labels = [[NSMutableArray alloc]init];
    int sum_width=20;
    int st_height=0;
    int word_width=0;
    int word_height=25;
    int sum_height=0;
    int space=0;
    Boolean rows = true;
    if(level==4){
        word_width =45;
    }else if(level==2){
        word_width = 25;
    }else{
        word_width = 50;
    }
    
        if(((frag_number+[wordList count]-1)*word_width)<=280){
            rows = false;
            st_height = 480;
            space = (280 - ((frag_number+[wordList count]-1)*word_width))/2;
            sum_width += space;
        }else{
            if([name count]==2){
                space = 5;
                sum_width = space+20;
                st_height = 445;
                
            }else if([name count]==3){
                st_height = 410;
            }
            word_height = 40;
            
        
        }
    sum_height += st_height;
    for(int h=0;h<[wordList count];h++){
    for(int i=0;i<[wordList[h] count];i++){
        DragLabel *label = [[DragLabel alloc]init];
        label.frame = CGRectMake(20+rand()%240,285+rand()%(st_height-315), word_width, 30);
        label.text = (NSString*) wordList[h][i];
        label.layer.borderColor = [UIColor blackColor].CGColor;
        label.layer.borderWidth = 1.5;
        
 
        label.userInteractionEnabled = YES;
        label.textAlignment = NSTextAlignmentCenter;
        [WorkSpace addSubview:label];
        [labels addObject:label];
        [self.WorkSpace addSubview:label];
    }
}
        //ターゲット作成
    for(int h=0;h<[wordList count];h++){
    for(int i=0;i<[wordList[h] count];i++){
        TargetView* viewA = [[TargetView alloc]initWithFrame:CGRectMake(sum_width, sum_height, word_width , 30)];
        viewA.userInteractionEnabled = YES;
        viewA.layer.borderColor = [UIColor blackColor].CGColor;
        viewA.layer.borderWidth = 1.5;
        viewA.correct_word = wordList[h][i];
        viewA.backgroundColor = [UIColor greenColor];
        [Targets addObject:viewA];
        [self.view addSubview:viewA];
        sum_width += viewA.frame.size.width;
    }if(rows){
        if([name count] == 2){
            sum_width = 300-5-(word_width*[wordList[1] count]);
        }else{
        if(h==0){
            sum_width = (280-(word_width*[wordList[1] count]))/2+20;
        }else if(h==1){
            sum_width = 300-5-(word_width*[wordList[2] count]);
        
        }
        }
        sum_height += word_height;
    }else{
        sum_width += word_width;
    }
    }
    
    
    for(int i=0;i<[labels count]; i++){
    
        DragLabel* dr = (DragLabel*)labels[i];
        dr.Views = Targets;
    
    }

}


- (IBAction)judge:(id)sender {
    int judge=0;
    for(int i=0;i<[Targets count];i++){
        TargetView* a= Targets[i];
        if(a.isCorrect){
            a.backgroundColor = [UIColor blueColor];
            judge++;
        }else{
        a.backgroundColor = [UIColor redColor];
        }
        
    }
        if(judge == [Targets count]){
            _collect.text = @"正解!";
            _collect.hidden = NO;
            [questions removeObjectAtIndex:0];
            [self performSelector:@selector(NextQuestion) withObject:nil afterDelay:1];
        }else{
            _collect.text = @"残念!";
            _collect.hidden = NO;
            [self performSelector:@selector(miss) withObject:nil afterDelay:1];
        
        }
        
    }

-(void)miss{
    _collect.hidden = YES;
}

- (IBAction)Hint:(id)sender {
    int trig=0;
    int i;
    TargetView *t=[[TargetView alloc]init];
    for(i=0;i<[Targets count];i++){
        t = Targets[i];
        if(!t.isCorrect){
            trig =1;
            break;
        }
    }
    
    if(trig){
        if(t.label != nil){
            DragLabel *d2 = t.label;
                    d2.frame = CGRectMake(d2.frame.origin.x, d2.frame.origin.y-40, d2.frame.size.width, d2.frame.size.height);
            d2.t_view = nil;
            d2.t_view.label = nil;
        }
        DragLabel *d = labels[i];
        if(d.isCorrect){
            for(int h=0;h<[labels count];h++){
                d = labels[h];
                if(!d.isCorrect & [t.correct_word isEqualToString:d.text]){
                    break;
                }
            
            }
            
        }
        d.frame = t.frame;
        d.t_view.label = nil;
        t.label = d;
        d.t_view = t;
        t.isCorrect = true;
        d.isCorrect =true;
    [self.view addSubview:d];
        
    }
}

- (IBAction)back:(id)sender {
    [labels removeAllObjects];
    [Targets removeAllObjects];
    [appDelegate resetQuestions];
    UITableViewController* select= [self.storyboard instantiateViewControllerWithIdentifier:@"select"];
   [self presentViewController:select animated:YES completion:nil];
    
}
    
-(void)NextQuestion{
    for(int i=0;i<[labels count];i++){
        [labels[i] removeFromSuperview];
    }
    for(int i=0;i<[Targets count];i++){
        [Targets[i] removeFromSuperview];
    }
    if([questions count]){
    [data_Set set:questions[0]];
    [self createQuestion:data_Set];
    }else{
    //全問正解後の処理
        isComplete = true;
        _text_area.text = @"おめでとう！全問正解だよ！ジャンル選択に戻って、違う問題にチャレンジしてみよう！";
        _collect.text = @"全問正解!";
        _JudgeButon.hidden = YES;
        _HintButton.hidden = YES;
    
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
