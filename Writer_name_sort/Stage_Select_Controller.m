//
//  Stage_Select_Controller.m
//  Writer_name_sort
//
//  Created by Shuhei on 2014/08/05.
//  Copyright (c) 2014年 三宅 修平. All rights reserved.
//

#import "Stage_Select_Controller.h"

@interface Stage_Select_Controller ()

@end

@implementation Stage_Select_Controller

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    appDelegate = [[UIApplication sharedApplication] delegate];
    data = [[NSMutableArray alloc]init];
   // [self dataBaseOpen];
    data = [appDelegate getCategory];
    CGRect Table_start_Loc = CGRectMake(0, 66, self.view.frame.size.width, 451);
    Table = [[UITableView alloc]initWithFrame:Table_start_Loc];
    Table.delegate = self;
    Table.dataSource = self;
    
    [self.view addSubview:Table];
    
    labels = [[NSMutableArray alloc]initWithCapacity:data.count];
    UIView *v = [[UIView alloc]initWithFrame:CGRectZero];
    v.backgroundColor = [UIColor clearColor];
    [Table setTableHeaderView:v];
    [Table setTableFooterView:v];
    int selectlevel;
    switch([appDelegate getLevel]){
        case 5 : selectlevel = 0; break;
        case 4 : selectlevel = 1; break;
        case 2 : selectlevel = 2; break;
            
    
    }
    
    _segment.selectedSegmentIndex = selectlevel;

}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return [data count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
 
    if(!cell){
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    cell.textLabel.text = [data objectAtIndex:indexPath.row];

    [labels addObject:cell];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   [tableView deselectRowAtIndexPath:indexPath animated:YES];
    int index = indexPath.row;
    [appDelegate setQuestions:index];
    [appDelegate setLevel:_segment.selectedSegmentIndex];
    UIViewController* main = [self.storyboard instantiateViewControllerWithIdentifier:@"main"];
   [self presentViewController:main animated:YES completion:nil];

}



- (void)didReceiveMemoryWarning

{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
