//
//  data_set.m
//  Writer_name_sort
//
//  Created by Shuhei on 2014/07/31.
//  Copyright (c) 2014年 三宅 修平. All rights reserved.
//

#import "data_set.h"
#import "ViewController.h"
@implementation data_set

@synthesize name;
@synthesize data;
@synthesize picture;


-(void)set:(NSMutableArray *)array
{

    [self setName:(NSString*)array[1]];
    self.data = (NSString*)array[2];
    NSString* picture_num = [NSString stringWithFormat:@"%@.jpg",(NSString*)array[3]];
   
    picture = [UIImage imageNamed:picture_num];
    

}



@end
