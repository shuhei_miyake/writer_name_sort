//
//  MyCell.m
//  Writer_name_sort
//
//  Created by Shuhei on 2014/08/05.
//  Copyright (c) 2014年 三宅 修平. All rights reserved.
//

#import "MyCell.h"


@implementation MyCell
@synthesize labels;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        isSelected = NO;
        
    }
    return self;
}


- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{

    [UIView animateWithDuration:0.5f
                          delay:0
                        options:UIViewAnimationCurveLinear
                     animations:^{[self extend];}
                     completion:^(BOOL finished){[self completion];}
     ];


}

-(void)extend{
    if(!isSelected){
        //ひらく
        Boolean frag = false;
        for(int i=0;i<labels.count;i++){
            UITableViewCell *others = labels[i];
            if(others == self){
                frag = true;
                continue;
            }
            if(!frag){continue;}else{
    [others setFrame:CGRectMake(others.frame.origin.x, others.frame.origin.y+100, others.frame.size.width, others.frame.size.height)];
            
            }
        }
    [self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.x, self.frame.size.width, self.frame.size.height+100)];
    }else{
        //折り畳む
        
    [self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, self.frame.size.height-100)];
    
    }
    isSelected = !isSelected;
}

-(void)completion{


}


@end
