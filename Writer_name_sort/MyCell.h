//
//  MyCell.h
//  Writer_name_sort
//
//  Created by Shuhei on 2014/08/05.
//  Copyright (c) 2014年 三宅 修平. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyCell : UITableViewCell{

    Boolean isSelected;
    CGRect Start_rect;
    CGRect big;
    UILabel *label;
    UIView *message;
    NSMutableArray *cells;
}

@property UILabel *label;
@property NSMutableArray *labels;
@end
